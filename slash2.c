#include <stdio.h>
int main() {

  char *outstr;
  size_t outsiz;
  FILE *outfile = open_memstream(&outstr, &outsiz);

  fprintf(outfile, "abc%c", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);
  fseek(outfile, -1, SEEK_CUR);
  fprintf(outfile, "/%s%c", "def", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);
  fseek(outfile, -1, SEEK_CUR);
  fprintf(outfile, "/%s%c", "ghi", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);

  rewind(outfile);

  fprintf(outfile, "jkl%c", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);
  fseek(outfile, -1, SEEK_CUR);
  fprintf(outfile, "/%s%c", "mno", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);
  fseek(outfile, -1, SEEK_CUR);
  fprintf(outfile, "/%s%c", "pqr", 0);
  fflush(outfile);
  printf("'%s'\n", outstr);

  return 0;
}

#if 0
glibc:

'abc'
'abc'
'abc'
'jkl'
'jkl'
'jkl'


musl:

'abc'
'abc/def'
'abc/def/ghi'
'jkl'
'jkl/mno'
'jkl/mno/pqr'
#endif
