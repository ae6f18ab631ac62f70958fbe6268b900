#include <stdio.h>
int main() {

  char *outstr;
  size_t outsiz;
  FILE *outfile = open_memstream(&outstr, &outsiz);

  fprintf(outfile, "abc");
  fflush(outfile);
  printf("'%s'\n", outstr);
  fprintf(outfile, "/%s", "def");
  fflush(outfile);
  printf("'%s'\n", outstr);
  fprintf(outfile, "/%s", "ghi");
  fflush(outfile);
  printf("'%s'\n", outstr);

  rewind(outfile);

  fprintf(outfile, "jkl");
  fflush(outfile);
  printf("'%s'\n", outstr);
  fprintf(outfile, "/%s", "mno");
  fflush(outfile);
  printf("'%s'\n", outstr);
  fprintf(outfile, "/%s", "pqr");
  fflush(outfile);
  printf("'%s'\n", outstr);

  return 0;
}

#if 0
glibc:

'abc'
'abc/def'
'abc/def/ghi'
'jkl'
'jkl/mno'
'jkl/mno/pqr'


musl:

'abc'
'abc/def'
'abc/def/ghi'
'jkl/def/ghi'
'jkl/mno/ghi'
'jkl/mno/pqr'
#endif
